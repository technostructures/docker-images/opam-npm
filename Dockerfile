ARG BASE_IMAGE

FROM ocaml/opam:${BASE_IMAGE}

USER root

RUN apk add --update nodejs npm

USER opam
